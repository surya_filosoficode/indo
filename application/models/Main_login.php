<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_login extends CI_Model{
    
    public function get_admin($where){
    	$this->db->select("id_admin, email, nama, nip, tk.id_toko as id_toko, tk.cabang as cabang, jenis_admin");
    	$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
        $admin = $this->db->get_where("admin ad", $where);
        return $admin;
    }    
}
?>