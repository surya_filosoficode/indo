<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_user extends CI_Model {

	public function get_laporan_all($id_admin){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		// $this->db->where("DATE_FORMAT(tgl, '%m') = ".$periode."");
		// $this->db->where("DATE_FORMAT(tgl, '%Y') = '".$th."'");
		$this->db->order_by("lp.tgl", "asc");
		$data = $this->db->get_where("lap_penjaualan lp", array("lp.id_admin"=>$id_admin))->result();
		return $data;
	}

	public function get_laporan_where($id_admin, $periode, $th){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->where("DATE_FORMAT(tgl, '%m') = ".$periode);
		$this->db->where("DATE_FORMAT(tgl, '%Y') = ".$th);
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("lap_penjaualan lp", array("lp.id_admin"=>$id_admin))->result();
		return $data;
	}

	public function get_laporan_where_id($id_penjualan){
		
		$this->db->join("admin ad", "lp.id_admin = ad.id_admin");
		$this->db->join("toko tk", "ad.id_toko = tk.id_toko");
		$this->db->order_by("tgl", "asc");
		$data = $this->db->get_where("lap_penjaualan lp", array("lp.id_lap"=>$id_penjualan))->row_array();
		return $data;
	}

	public function insert_laporan($data){
		$data = $this->db->insert("lap_penjaualan", $data);
		return $data;
	}

	public function update_laporan($data, $where){
		$data = $this->db->update("lap_penjaualan", $data, $where);
		return $data;
	}

	public function delete_laporan($where){
		$delete = $this->db->delete("lap_penjaualan", $where);
		return $delete;
	}

}
