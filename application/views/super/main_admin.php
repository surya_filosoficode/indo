            <?php
                if(!empty($toko)){
                    $str_option_toko = "";
                    foreach ($toko as $r_toko => $v_toko) {
                        $str_option_toko .= "<option value=\"".$v_toko->id_toko."\">".$v_toko->cabang." - ".$v_toko->alamat."</option>";
                    }
                }
            ?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Beranda</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <div class="row">
                    <!-- <div class="row"> -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <h4 class="card-title">Data Admin</h4>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-info waves-effect text-right" data-toggle="modal" data-target="#insert_modal">Tambah Admin</button>
                                        </div>
                                    </div>
                                    
                                    <div class="table-responsive m-t-40">
                                        <table id="myTable" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Id Admin</th>
                                                    <th>Username</th>
                                                    <th>Nama</th>
                                                    
                                                    <th>Toko Cabang</th>
                                                    <th>Alamat</th>
                                                    <th>Jenis Admin</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(!empty($admin)){
                                                        foreach ($admin as $r_admin => $v_admin) {
                                                            $str_jenis = "Super Admin";
                                                            if($v_admin->jenis_admin == 1){
                                                                $str_jenis = "Admin Toko";
                                                            }
                                                            echo "<tr>
                                                                    <td>".$v_admin->id_admin."</td>
                                                                    <td>".$v_admin->email."</td>
                                                                    <td>".$v_admin->nama."</td>
                                                                    
                                                                    <td>".$v_admin->cabang."</td>
                                                                    <td>".$v_admin->alamat."</td>

                                                                    <td>".$str_jenis."</td>
                                                                    <td>
                                                                        <a href=\"#\" onclick=\"up_admin('".$v_admin->id_admin."')\"><i class=\"btn btn-info\"><i class=\"fa fa-pencil-square-o\"></i></i></a>
                                                                        <a href=\"#\" onclick=\"del_admin('".$v_admin->id_admin."')\"><i class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i></i></a>
                                                                    </td>
                                                                </tr>";
                                                        }
                                                    }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    <!-- </div> -->
                </div>
                <!-- End Row -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
                                <div class="modal fade bs-example-modal-lg" id="insert_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Tambah Data Admin</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form action="<?= base_url()."super/mainsuper/insert_admin"?>" method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <br>
                                                <div class="form-body">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Nama</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">NIP</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="NIP" id="nip" name="nip" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">User Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="user" name="user" placeholder="User Name" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Jenis Admin</label>
                                                                <div class="col-md-9">
                                                                    <select name="jenis_admin" id="jenis_admin" class="form-control">
                                                                        <option value="0">Super Admin</option>
                                                                        <option value="1">Admin Cabang</option>
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-2">Jenis Toko</label>
                                                                <div class="col-md-10">
                                                                    <select name="jenis_toko" id="jenis_toko" class="form-control">
                                                                        <?= $str_option_toko;?>
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" placeholder="******" id="pass" name="pass" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Ulangi Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" placeholder="******" id="repass" name="repass" required="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    
                                                    
                                                </div>    
                                                                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger waves-effect text-left">Sipman</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->



                                <div class="modal fade bs-example-modal-lg" id="update_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Ubah Data Admin</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form action="<?= base_url()."super/mainsuper/up_admin"?>" method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <br>
                                                <div class="form-body">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Nama</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="up_nama" name="nama" placeholder="Nama" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">NIP</label>
                                                                <div class="col-md-9">
                                                                    <input type="number" class="form-control" placeholder="NIP" id="up_nip" name="nip" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">User Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="up_user" name="user" placeholder="User Name" required="">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">Jenis Admin</label>
                                                                <div class="col-md-9">
                                                                    <select name="jenis_admin" id="up_jenis_admin" class="form-control">
                                                                        <option value="0">Super Admin</option>
                                                                        <option value="1">Admin Cabang</option>
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-2">Jenis Toko</label>
                                                                <div class="col-md-10">
                                                                    <select name="jenis_toko" id="up_jenis_toko" class="form-control">
                                                                        <?= $str_option_toko;?>
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                
                                                        <div class="col-md-6" hidden="">
                                                            <div class="form-group row">
                                                                <label class="control-label text-left col-md-3">id_admin</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" placeholder="******" id="up_id_admin" name="id_admin" required="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                    
                                                </div>    
                                                                                               
                                            </div>
                                            <div class="modal-footer">
                                                <!-- <input type="submit" name="ubah" class="btn btn-danger text-left"" value="Ubah"> -->
                                                <button type="submit" id="btn_ubah" class="btn btn-danger waves-effect text-left">Ubah</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->



                                <script type="text/javascript">
                                    function del_admin(id_admin){
                                        var conf = confirm("Apakah anda yakin untuk menghapus "+id_admin+" ?, Jika anda tekan OK, maka seluruh data yang berhubungan dengan "+id_admin+" akan terhapus semau.. !!!!!!! ");

                                        if(conf){
                                            window.location.href = "<?= base_url()."super/mainsuper/delete_admin/";?>"+id_admin;
                                        }else{

                                        }
                                    }


                                    function up_admin(id_admin){
                                        clear_mod_up();
                                        // console.lo

                                        var data_main =  new FormData();
                                        data_main.append('id_admin', id_admin);    
                                        $.ajax({
                                            url: "<?php echo base_url()."/super/mainsuper/index_up_admin/";?>", // point to server-side PHP script 
                                            dataType: 'html',  // what to expect back from the PHP script, if anything
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: data_main,                         
                                            type: 'post',
                                            success: function(res){
                                                // console.log(res);
                                                res_update(res);
                                                // $("#out_up_mhs").html(res);
                                            }
                                        });
                                        $("#update_modal").modal('show');
                                        
                                    }

                                    function res_update(res){
                                        var data = JSON.parse(res);
                                        // console.log(data);

                                        if(data.status){
                                            var main_data = data.val;
                                            console.log(main_data);
                                            
                                            $("#up_nama").val(main_data.nama);
                                            $("#up_nip").val(main_data.nip);
                                            $("#up_user").val(main_data.email);
                                            // $("#jenis_admin").val("");
                                            // $("#up_nama").val("");
                                            // $("#up_pass").val(main_data.pas);
                                            // $("#up_pass").val("");
                                            $("#up_id_admin").val(main_data.id_admin);

                                            $("#up_jenis_toko").val(main_data.id_toko);
                                            $("#up_jenis_admin").val(main_data.jenis_admin);
                                            
                                        }else{
                                            clear_mod_up();
                                        }
                                    }

                                    function clear_mod_up(){
                                        $("#up_nama").val("");
                                        $("#up_nip").val("");
                                        $("#up_user").val("");
                                        // $("#jenis_admin").val("");
                                        // $("#up_nama").val("");
                                        $("#up_pass").val("");
                                        $("#up_repass").val("");
                                        $("#up_id_admin").val("");

                                    }


                                    // $("#btn_ubah").click(function(){
                                       
                                    //     var data_main =  new FormData();
                                    //     data_main.append('id_admin', $("#up_id_admin").val());  
                                    //     data_main.append('nama', $("#up_nama").val());  
                                    //     data_main.append('nip', $("#up_nip").val());  
                                    //     data_main.append('user', $("#up_user").val());  
                                    //     data_main.append('jenis_toko', $("#up_jenis_toko").val());  
                                    //     data_main.append('jenis_admin', $("#up_jenis_admin").val());  


                                    //     $.ajax({
                                    //         url: "<?php echo base_url()."/super/mainsuper/up_admin/";?>", // point to server-side PHP script 
                                    //         dataType: 'html',  // what to expect back from the PHP script, if anything
                                    //         cache: false,
                                    //         contentType: false,
                                    //         processData: false,
                                    //         data: data_main,                         
                                    //         type: 'post',
                                    //         success: function(res){

                                    //             console.log(res);
                                    //             // res_update(res);
                                    //             // $("#out_up_mhs").html(res);
                                    //         }
                                    //     });
                                    // });
                                </script>