            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Beranda</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <div class="row">
                    <div class="col-12 m-t-30">
                        <div id="code2" class="collapse highlight">
                            <pre class="prettyprint"><code class="language-html" data-lang="html">
                            <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card"</span><span class="nt">&gt;</span>
                              <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card-header"</span><span class="nt">&gt;</span>
                                Featured
                              <span class="nt">&lt;/div&gt;</span>
                              <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card-body"</span><span class="nt">&gt;</span>
                                <span class="nt">&lt;h4</span> <span class="na">class=</span><span class="s">"card-title"</span><span class="nt">&gt;</span>Special title treatment<span class="nt">&lt;/h4&gt;</span>
                                <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"card-text"</span><span class="nt">&gt;</span>With supporting text below as a natural lead-in to additional content.<span class="nt">&lt;/p&gt;</span>
                                <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"#"</span> <span class="na">class=</span><span class="s">"btn btn-primary"</span><span class="nt">&gt;</span>Go somewhere<span class="nt">&lt;/a&gt;</span>
                              <span class="nt">&lt;/div&gt;</span>
                            <span class="nt">&lt;/div&gt;</span></code>
                            </pre>
                        </div>

                        <?php
                            $nama  = $this->session->userdata("indo_log")["nama"];
                            $cabang = $this->session->userdata("indo_log")["cabang"];
                            // $alamat = $this->session->userdata("indo_log")["alamat"];
                        ?>
                        <!-- Card -->
                        <div class="card">
                            <div class="card-header">
                                WELCOME
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Selamat Datang <b><?= $nama; ?> Admin Pusat</b></h4>
                                <p class="card-text">Salam Super, Semoga Anda menikmati pekerjaan anda hari ini</p>
                                <!-- <a href="#" class="btn btn-primary"></a> -->
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
           