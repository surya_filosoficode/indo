<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainreportsuper extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->model("super/main_super", "ms");
		$this->load->library("response_message");

		if($this->session->userdata("indo_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("indo_log")["jenis_admin"] != 0){
                redirect(base_url());
            }
        }

	}

	public function index(){
		$data["page"] = "laporan";
		$this->load->view('index_super',$data);
	}

	public function index_penjualan(){
		$data["page"] = "penjualan";
		$data["toko"] = $this->ms->get_toko(); 
		$this->load->view('index_super',$data);
	}

#==================================================================================================== Penjualan =======================================================================================
#------------------------------------------------------------------------------------------------------- Main -------------------------------------------------------------------------------------------
#==================================================================================================== Penjualan =======================================================================================
	
	public function show_report(){
		// $id_admin = "AD2018100002";
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");
		$cabang = $this->input->post("cabang");

		$data["penjualan"] = array();
		for ($i=0; $i < 3; $i++) { 
			$data["penjualan"][$i] = $this->ms->get_super_laporan_where($cabang, $periode-$i, $th);
		}
		$this->load->view("user/laporan_all", $data);
	}

	public function show_tbl(){
		// $id_admin = $this->session->userdata("indo_log")["id_admin"];
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");
		$cabang = $this->input->post("cabang");

		// $periode = 5;
		// $th = 2018;
		// $cabang = 2;

		$data["penjualan"] = $this->ms->get_super_laporan_where($cabang, $periode, $th);		
		

		print_r(json_encode($data));
		// $this->load->view("super/tbl_laporan_all", $data);
	}

#==================================================================================================== Penjualan =======================================================================================
#------------------------------------------------------------------------------------------------------- Main -------------------------------------------------------------------------------------------
#==================================================================================================== Penjualan =======================================================================================


}
