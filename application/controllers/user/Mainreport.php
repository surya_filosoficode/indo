<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainreport extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->library("response_message");

		if($this->session->userdata("indo_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("indo_log")["jenis_admin"] != 1){
                redirect(base_url());
            }
        }
	}

	public function index(){
		$data["page"] = "laporan";
		$this->load->view('index_admin',$data);
	}

#==================================================================================================== Penjualan =======================================================================================
#------------------------------------------------------------------------------------------------------- Main -------------------------------------------------------------------------------------------
#==================================================================================================== Penjualan =======================================================================================
	public function show_report(){
		$id_admin = $this->session->userdata("indo_log")["id_admin"];
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");
		$data["penjualan"] = array();
		for ($i=0; $i < 3; $i++) { 
			$data["penjualan"][$i] = $this->mu->get_laporan_where($id_admin, $periode-$i, $th);
		}
		// print_r("<pre>");
		// print_r($periode);echo " - ";
		// print_r($th);echo " - ";
		// print_r($id_admin);echo " <br> ";

		// print_r($data);
		$this->load->view("user/laporan_all", $data);
	}

	public function show_tbl(){
		$id_admin = $this->session->userdata("indo_log")["id_admin"];
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");

		$data = $this->mu->get_laporan_where($id_admin, $periode, $th);
		
		// foreach ($data as $r_data => $v_data) {
		// 	echo "<tr>
  //                   <td>".$v_data->tgl."</td>
  //                   <td align=\"right\">Rp. ".number_format($v_data->sales_net, 2,'.', ',')."</td>
  //                   <td align=\"right\">".number_format($v_data->struk, 0,'.', ',')."</td>
  //                   <td align=\"right\">Rp. ".number_format($v_data->pergantian, 2,'.', ',')."</td>
  //                   <td align=\"right\">Rp. ".number_format($v_data->variance, 2,'.', ',')."</td>
  //                   <td align=\"right\">Rp. ".number_format($v_data->discount, 2,'.', ',')."</td>
  //                   <td align=\"right\">Rp. ".number_format($v_data->dsi_ini, 2,'.', ',')."</td>
  //               </tr>";
		// }

		print_r(json_encode($data));
		// $this->load->view("user/laporan_all", $data);
	}

#==================================================================================================== Penjualan =======================================================================================
#------------------------------------------------------------------------------------------------------- Main -------------------------------------------------------------------------------------------
#==================================================================================================== Penjualan =======================================================================================


}
