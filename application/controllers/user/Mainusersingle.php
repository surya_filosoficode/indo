 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainusersingle extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->library("response_message");

		if($this->session->userdata("indo_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("indo_log")["jenis_admin"] != 1){
                redirect(base_url());
            }
        }
	}

#=========================================================================================
#---------------------------------------main_single---------------------------------------
#=========================================================================================
	public function index_single(){
		$id_admin = $this->session->userdata("indo_log")["id_admin"];
		$array_alpha = array(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9);

		$data["page"] = "single";
		$list_penjualan = $this->mu->get_laporan_all($id_admin);

		$data_hasil = array();
		$end_sales_net = 0;
		$end_tgl = 0;
		$end_row = 0;

		$data_chart = array();
		$data_series = "var seriesx = chart.series.push(new am4charts.LineSeries());
                seriesx.dataFields.valueY = \"data_main\";
                seriesx.dataFields.categoryX = \"year\";
                seriesx.name = \"Data Real\";
                seriesx.strokeWidth = 3;
                seriesx.bullets.push(new am4charts.CircleBullet());
                seriesx.tooltipText = \"Place taken by {name} in {categoryX}: {valueY}\";
                seriesx.legendSettings.valueText = \"{valueY}\";
                seriesx.visible  = false;";

		$count_penjualan = count($list_penjualan);
		$chart_st = $count_penjualan - 27;

		foreach ($array_alpha as $r_array_alpha => $v_array_alpha) {
			$data_series .= "var series".$r_array_alpha." = chart.series.push(new am4charts.LineSeries());
                series".$r_array_alpha.".dataFields.valueY = \"alpha_".$v_array_alpha."\";
                series".$r_array_alpha.".dataFields.categoryX = \"year\";
                series".$r_array_alpha.".name = \"alpha_".$v_array_alpha."\";
                series".$r_array_alpha.".strokeWidth = 3;
                series".$r_array_alpha.".bullets.push(new am4charts.CircleBullet());
                series".$r_array_alpha.".tooltipText = \"Place taken by {name} in {categoryX}: {valueY}\";
                series".$r_array_alpha.".legendSettings.valueText = \"{valueY}\";
                series".$r_array_alpha.".visible  = false;";
		}

		$no = 0;

		foreach ($list_penjualan as $r_list_penjualan => $v_list_penjualan) {
			if($r_list_penjualan == 0){
				$data_hasil[$r_list_penjualan]["base_data"] = $list_penjualan[$r_list_penjualan]->sales_net;
				$data_hasil[$r_list_penjualan]["tgl"] = $list_penjualan[$r_list_penjualan]->tgl;
			}else{
				$data_hasil[$r_list_penjualan]["base_data"] = $list_penjualan[$r_list_penjualan]->sales_net;
				$data_hasil[$r_list_penjualan]["tgl"] 		= $list_penjualan[$r_list_penjualan]->tgl;
			}

			foreach ($array_alpha as $key => $value) {
				if($r_list_penjualan == 0){
					$data_hasil[$r_list_penjualan]["item"][$key] = $v_list_penjualan->sales_net;
				}else{
					$data_hasil[$r_list_penjualan]["item"][$key] = ((float)$value*(float)$list_penjualan[$r_list_penjualan-1]->sales_net) + ((1 - (float)$value)*$data_hasil[$r_list_penjualan-1]["item"][$key]);
				}

				if($r_list_penjualan >= $chart_st){
					$data_chart[$no]["alpha_".$value] = (int)$data_hasil[$r_list_penjualan]["item"][$key];
				}
			}

			if($r_list_penjualan >= $chart_st){
				$data_chart[$no]["year"] = $data_hasil[$r_list_penjualan]["tgl"];
				$data_chart[$no]["data_main"] = $data_hasil[$r_list_penjualan]["base_data"];
				$no++;
			}

			$end_sales_net = $v_list_penjualan->sales_net;
			$end_tgl = $v_list_penjualan->tgl;
			$end_row = $r_list_penjualan;

			
		}

		$data_forecast = array();
		$max_forecast = 3;

		if($list_penjualan){
			for ($i=1; $i <= $max_forecast; $i++) { 
				$data_forecast[$i]["base_data"] = $end_sales_net;
				$data_forecast[$i]["tgl"] 		= date('Y-m-d', strtotime('+'.$i.' days', strtotime($end_tgl)));

				foreach ($array_alpha as $key => $value) {
					// print_r("<pre>");
					// print_r($value);

					if($i == 1){
						$data_forecast[$i]["item"][$key] = ((float)$value*(float)$list_penjualan[$end_row]->sales_net) + ((1 - (float)$value)*$data_hasil[$end_row]["item"][$key]);
					}else{
						$data_forecast[$i]["item"][$key] = ((float)$value*(float)$list_penjualan[$end_row]->sales_net) + ((1 - (float)$value)*$data_forecast[$i-1]["item"][$key]);
						// print_r($data_forecast[$i-1]["item"][$key]);
					}

					$data_chart[$no]["alpha_".$value] = (int)$data_forecast[$i]["item"][$key];
					
				}
				$data_chart[$no]["year"] 		= $data_forecast[$i]["tgl"];
				$data_chart[$no]["data_main"] 	= "";
				$no++;
				// print_r("<hr>");
			}
		}
			

		$data["array_alpha"] 	= $array_alpha;
		$data["data_analisa"] 	= $data_hasil;
		$data["data_forecast"] 	= $data_forecast;

		$data["data_chart"] 	= json_encode($data_chart);
		$data["data_series"] 	= $data_series;

		// print_r("<pre>");
		// print_r($list_penjualan);
		// print_r($data_hasil);

		$this->load->view('index_admin',$data);
	}
#=========================================================================================
#---------------------------------------main_single---------------------------------------
#=========================================================================================

#=========================================================================================
#---------------------------------------search_single-------------------------------------
#=========================================================================================
	public function index_search(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");

		$id_admin = $this->session->userdata("indo_log")["id_admin"];
		
		$array_alpha = array(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9);

		$data["page"] = "single";
		$list_penjualan = $this->mu->get_laporan_all($id_admin);
		if($tipe_choose == "1"){
			$list_penjualan = $this->mu->get_laporan_where($id_admin, $periode, $th);	
		}

		$data_hasil = array();
		$end_sales_net = 0;
		$end_tgl = 0;
		$end_row = 0;

		$data_chart = array();
		$data_series = "var seriesx = chart.series.push(new am4charts.LineSeries());
                seriesx.dataFields.valueY = \"data_main\";
                seriesx.dataFields.categoryX = \"year\";
                seriesx.name = \"Data Real\";
                seriesx.strokeWidth = 3;
                seriesx.bullets.push(new am4charts.CircleBullet());
                seriesx.tooltipText = \"Place taken by {name} in {categoryX}: {valueY}\";
                seriesx.legendSettings.valueText = \"{valueY}\";
                seriesx.visible  = false;";


        $count_penjualan = count($list_penjualan);
		$chart_st = $count_penjualan - 27;

		foreach ($array_alpha as $r_array_alpha => $v_array_alpha) {
			$data_series .= "var series".$r_array_alpha." = chart.series.push(new am4charts.LineSeries());
                series".$r_array_alpha.".dataFields.valueY = \"alpha_".$v_array_alpha."\";
                series".$r_array_alpha.".dataFields.categoryX = \"year\";
                series".$r_array_alpha.".name = \"alpha_".$v_array_alpha."\";
                series".$r_array_alpha.".strokeWidth = 3;
                series".$r_array_alpha.".bullets.push(new am4charts.CircleBullet());
                series".$r_array_alpha.".tooltipText = \"Place taken by {name} in {categoryX}: {valueY}\";
                series".$r_array_alpha.".legendSettings.valueText = \"{valueY}\";
                series".$r_array_alpha.".visible  = false;";
		}

		$no = 0;

		foreach ($list_penjualan as $r_list_penjualan => $v_list_penjualan) {
			if($r_list_penjualan == 0){
				$data_hasil[$r_list_penjualan]["base_data"] = $list_penjualan[$r_list_penjualan]->sales_net;
				$data_hasil[$r_list_penjualan]["tgl"] = $list_penjualan[$r_list_penjualan]->tgl;
			}else{
				$data_hasil[$r_list_penjualan]["base_data"] = $list_penjualan[$r_list_penjualan-1]->sales_net;
				$data_hasil[$r_list_penjualan]["tgl"] 		= $list_penjualan[$r_list_penjualan]->tgl;
			}

			foreach ($array_alpha as $key => $value) {
				if($r_list_penjualan == 0){
					$data_hasil[$r_list_penjualan]["item"][$key] = $v_list_penjualan->sales_net;
				}else{
					$data_hasil[$r_list_penjualan]["item"][$key] = ((float)$value*(float)$list_penjualan[$r_list_penjualan-1]->sales_net) + ((1 - (float)$value)*$data_hasil[$r_list_penjualan-1]["item"][$key]);
				}

				if($tipe_choose == "0"){
					if($r_list_penjualan >= $chart_st){
						$data_chart[$no]["alpha_".$value] = (int)$data_hasil[$r_list_penjualan]["item"][$key];
					}
				}else{
					$data_chart[$no]["alpha_".$value] = (int)$data_hasil[$r_list_penjualan]["item"][$key];
				}
			}

			if($tipe_choose == "0"){
				if($r_list_penjualan >= $chart_st){
					$data_chart[$no]["year"] = $data_hasil[$r_list_penjualan]["tgl"];
					$data_chart[$no]["data_main"] = $data_hasil[$r_list_penjualan]["base_data"];
					$no++;
				}	
			}else{
				$data_chart[$no]["year"] = $data_hasil[$r_list_penjualan]["tgl"];
				$data_chart[$no]["data_main"] = $data_hasil[$r_list_penjualan]["base_data"];
				$no++;
			}
			
			$end_sales_net = $v_list_penjualan->sales_net;
			$end_tgl = $v_list_penjualan->tgl;
			$end_row = $r_list_penjualan;

			
		}

		$data_forecast = array();
		$max_forecast = 3;

		if($list_penjualan){
			for ($i=1; $i <= $max_forecast; $i++) { 
				$data_forecast[$i]["base_data"] = $end_sales_net;
				$data_forecast[$i]["tgl"] 		= date('Y-m-d', strtotime('+'.$i.' days', strtotime($end_tgl)));

				foreach ($array_alpha as $key => $value) {
					// print_r("<pre>");
					// print_r($value);

					if($i == 1){
						$data_forecast[$i]["item"][$key] = ((float)$value*(float)$list_penjualan[$end_row]->sales_net) + ((1 - (float)$value)*$data_hasil[$end_row]["item"][$key]);
					}else{
						$data_forecast[$i]["item"][$key] = ((float)$value*(float)$list_penjualan[$end_row]->sales_net) + ((1 - (float)$value)*$data_forecast[$i-1]["item"][$key]);
						// print_r($data_forecast[$i-1]["item"][$key]);
					}

					$data_chart[$no]["alpha_".$value] = (int)$data_forecast[$i]["item"][$key];
					
				}
				$data_chart[$no]["year"] 		= $data_forecast[$i]["tgl"];
				$data_chart[$no]["data_main"] 	= "";
				$no++;
				// print_r("<hr>");
			}
		}
			

		$data["array_alpha"] 	= $array_alpha;
		$data["data_analisa"] 	= $data_hasil;
		$data["data_forecast"] 	= $data_forecast;

		$data["data_chart"] 	= json_encode($data_chart);
		$data["data_series"] 	= $data_series;

		// print_r("<pre>");
		// print_r(json_encode($data_chart));

		$this->load->view('index_admin',$data);
	}
#=========================================================================================
#---------------------------------------search_single-------------------------------------
#=========================================================================================

#=========================================================================================
#---------------------------------------report_forecasting--------------------------------
#=========================================================================================

	public function show_report(){
		$tipe_choose 	= $this->input->post("tipe_choose");
		$periode 		= $this->input->post("periode");
		$th 			= $this->input->post("th");

		$id_admin = $this->session->userdata("indo_log")["id_admin"];
		$array_alpha = array(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9);

		$data["page"] = "single";
		$list_penjualan = $this->mu->get_laporan_all($id_admin);
		if($tipe_choose == "1"){
			$list_penjualan = $this->mu->get_laporan_where($id_admin, $periode, $th);	
		}

		$data_hasil = array();
		$end_sales_net = 0;
		$end_tgl = 0;
		$end_row = 0;
		$data_chart = array();
		$data_series = "";

		$no = 0;
		foreach ($list_penjualan as $r_list_penjualan => $v_list_penjualan) {
			if($r_list_penjualan == 0){
				$data_hasil[$r_list_penjualan]["base_data"] = $list_penjualan[$r_list_penjualan]->sales_net;
				$data_hasil[$r_list_penjualan]["tgl"] = $list_penjualan[$r_list_penjualan]->tgl;
			}else{
				$data_hasil[$r_list_penjualan]["base_data"] = $list_penjualan[$r_list_penjualan-1]->sales_net;
				$data_hasil[$r_list_penjualan]["tgl"] 		= $list_penjualan[$r_list_penjualan]->tgl;
			}

			$data_chart[$no]["year"] = $data_hasil[$r_list_penjualan]["tgl"]; 

			foreach ($array_alpha as $key => $value) {
				if($r_list_penjualan == 0){
					$data_hasil[$r_list_penjualan]["item"][$key] = $v_list_penjualan->sales_net;
				}else{
					$data_hasil[$r_list_penjualan]["item"][$key] = ((float)$value*(float)$list_penjualan[$r_list_penjualan-1]->sales_net) + ((1 - (float)$value)*$data_hasil[$r_list_penjualan-1]["item"][$key]);
				}

				$data_chart[$no]["alpha-".$value] = "0"; 
			}

			$end_sales_net = $v_list_penjualan->sales_net;
			$end_tgl = $v_list_penjualan->tgl;
			$end_row = $r_list_penjualan;

			$no++;
		}

		$data_forecast = array();
		$max_forecast = 3;

		if($list_penjualan){
			for ($i=1; $i <= $max_forecast; $i++) { 
				$data_forecast[$i]["base_data"] = $end_sales_net;
				$data_forecast[$i]["tgl"] 		= date('Y-m-d', strtotime('+'.$i.' days', strtotime($end_tgl)));

				foreach ($array_alpha as $key => $value) {
					// print_r("<pre>");
					// print_r($value);

					if($i == 1){
						$data_forecast[$i]["item"][$key] = ((float)$value*(float)$list_penjualan[$end_row]->sales_net) + ((1 - (float)$value)*$data_hasil[$end_row]["item"][$key]);
					}else{
						$data_forecast[$i]["item"][$key] = ((float)$value*(float)$list_penjualan[$end_row]->sales_net) + ((1 - (float)$value)*$data_forecast[$i-1]["item"][$key]);
						// print_r($data_forecast[$i-1]["item"][$key]);
					}
				}

				// print_r("<hr>");
			}
		}
			

		$data["array_alpha"] 	= $array_alpha;
		$data["data_analisa"] 	= $data_hasil;
		$data["data_forecast"] 	= $data_forecast;

		

		$this->load->view('user/forecast_cetak', $data);
	}

#=========================================================================================
#---------------------------------------report_forecasting--------------------------------
#=========================================================================================
}
